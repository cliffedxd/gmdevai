﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : MonoBehaviour
{
    Animator anim;
    public GameObject player;
    public GameObject bullet;
    public GameObject turret;
    public float currentHP;
    public float maxHp = 10;
    public GameObject GetPlayer()
    {
        return player;
    }

    void Start()
    {
        anim = this.GetComponent<Animator>();
        currentHP = maxHp;
    }

    void Update()
    {
        anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
        CheckDeath();
        CheckCriticalHP();
    }

    void Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }

    public void StartFiring()
    {
        InvokeRepeating("Fire", 0.5f, 0.5f);
    }

    public void StopFiring()
    {
        CancelInvoke("Fire");
    }

    public void TakeDamage()
    {
        currentHP--;
    }

    public void Death()
    {
        Destroy(gameObject);
    }

    public void CheckDeath()
    {
        if(currentHP <= 0)
            Death();
    }

    public void CheckCriticalHP()
    {
        if(currentHP <= (maxHp * 0.2))
            anim.SetBool("isHpLow", true);
        else
            anim.SetBool("isHpLow", false);
    }
}
