﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankManager : MonoBehaviour
{
    public GameObject redTank;
    public GameObject blueTank;
    public GameObject greenTank;
    FollowPath redFP, blueFP, greenFP;
    // Start is called before the first frame update
    void Start()
    {
        redFP = redTank.GetComponent<FollowPath>();
        blueFP = blueTank.GetComponent<FollowPath>();
        greenFP = greenTank.GetComponent<FollowPath>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnableRedTank()
    {
        redFP.enabled = true;
        blueFP.enabled = false;
        greenFP.enabled = false;
    }

    public void EnableBlueTank()
    {
        blueFP.enabled = true;
        redFP.enabled = false;
        greenFP.enabled = false;
    }

    public void EnableGreenTank()
    {
        greenFP.enabled = true;
        blueFP.enabled = false;
        redFP.enabled = false;
    }
}
